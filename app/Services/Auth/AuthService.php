<?php


namespace App\Services\Auth;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public function saveUser($request)
    {
        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'user_name' => $request->user_name,
           'password' => Hash::make($request->password),
        ]);
       $user->save();
       return $user;
    }

    public function refreshToken($request)
    {
        $client = new \GuzzleHttp\Client();
        $data = $client->post(config('services.passport.login_endpoint'), [
            "form_params" => [
                'refresh_token' => $request->refresh_token,
                "client_secret" => config('services.passport.client_secret'),
                "grant_type" => "refresh_token",
                "client_id" => config('services.passport.client_id'),
                'scope' => '',
            ]
        ]);
        return $data->getBody()->getContents();
    }

    public function getAccessToken($request)
    {
        $client = new \GuzzleHttp\Client();
        $data = $client->post(config('services.passport.login_endpoint'), [
            "form_params" => [
                "client_secret" => config('services.passport.client_secret'),
                "grant_type" => "password",
                "client_id" => config('services.passport.client_id'),
                "username" => $request->email,
                "password" => $request->password
            ]
        ]);
        return $data->getBody()->getContents();
    }
    public function resetPassword($request)
    {
        $user = User::where('email',$request->email)->first();
        if($user)
        {
            if($request->email== $user->email)
            {
                $user = User::find($user->id);
                $user->password = Hash::make($request->password);
                $user->save();
                return true;
            }
            else
            {
                return false;
            }
        }
    } 
}
