<?php


namespace App\Services\Profile;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class UserProfileService
{
    public $photo;
    public function saveUserProfile($request)
    {
        $user = Auth::user();
        if($user)
        {
            if ($user->user_profile()->exists()) {
                $user->user_profile()->update($request->except(['image']));
            }
            else {
                $user->user_profile()->create($request->except(['image']));
            }
            if ($request->has('image')) {
                $image_url = $this->saveUserProfileImage($request);
                $user->user_profile['image'] = $image_url;
            }
            
            return User::with('user_profile')->find($user->id);
        }
    }
    public function saveUserProfileImage($request){
      
        $user = Auth::user();
        if($user) {
            $photoPath = $request->image;
            if (!is_string($request->image) && $request->image) {
             
                $name      = uniqid() . '.' . $request->image->getClientOriginalExtension();
                $photoPath = $request->image->storeAs('user', $name, [
                    'disk' => 'avatars',
                ]);
            }
            if (!empty($request->image) && !is_string($request->image)) {
                $this->photo = $photoPath;
            }
            if (!is_string($this->photo)) {
                $data['photo'] = $photoPath;
            }
            $user = User::find($user->id);
            if ($user->user_profile()->exists()) {
                $user->user_profile->image = $photoPath;
                $user->user_profile->save(); 
            } else {
                $user->user_profile()->create([
                    'image'=>$photoPath
                ]);
            } 
            return $photoPath;
        }
    }
    public function userProfile()
    {
        $user = Auth::user();
        if($user) {
            if ($user->user_profile()->exists())
            {
                return User::with('user_profile')->find($user->id);
            }
        }
    }
}