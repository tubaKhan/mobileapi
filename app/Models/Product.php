<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_name',
        'category_id',
        'price',
        'status'
    ];
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
    public function scopeApplyFilter($query,array $filters)
    {
        
        $filters=collect($filters);
        if( $filters->get('product_name'))
        {
            $query->WhereProductName($filters->get('product_name'));
        }
        if( $filters->get('category_id') )
        {
            $query->WhereCategoryId($filters->get('category_id'));
        }
    }
    public function scopeWhereProductName($query,$product_name)
    {
        $query->where('product_name','LIkE',$product_name.'%');
        
    }
    public function scopeWhereCategoryId($query,$category_id)
    {
        $query->where('category_id',$category_id);
        
    }

}
