<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];
    public function products()
    {
        return $this->hasMany(Product::class,'category_id', 'id');
    }
    public function scopeApplyFilter($query,array $filters)
    {
        
        $filters=collect($filters);
        if( $filters->get('category_name'))
        {
            $query->WhereCategoryName($filters->get('category_name'));
        }
    }
    public function scopeWhereCategoryName($query,$category_name)
    {
        $query->where('name','LIkE',$category_name.'%');
        
    }

}
