<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class UserProfile extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'phone_number',
        'user_id',
        'country'
    ];
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');

    }
}
