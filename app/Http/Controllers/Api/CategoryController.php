<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        try {
            $categories=Category::ApplyFilter($request->only(['category_name']))->paginate(10);
            return $this->success('Categories', $categories, 200);
        }
        catch (\Exception $exception) {
            return $this->failure('Internal Server Error! Something went wrong.', 500);
        }
    }
}
