<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products=Product::ApplyFilter($request->only(['product_name','category_id']))->paginate(10);
        try {
            $products=Product::ApplyFilter($request->only(['product_name','category_id']))->paginate(10);
            return $this->success('Products', $products, 200);
        }
        catch (\Exception $exception) {
            return $this->failure('Internal Server Error! Something went wrong.', 500);
        }
    }
    public function categoryWiseProducts(Request $request)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
        ]);
        try {
            $CategoryWiseProducts=Product::ApplyFilter($request->only(['category_id']))->paginate(10);
            return $this->success('Category Wise Products', $CategoryWiseProducts, 200);
        }
        catch (\Exception $exception) {
            return $this->failure('Internal Server Error! Something went wrong.', 500);
        }
    }
}
