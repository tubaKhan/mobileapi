<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Services\Auth\AuthService;
use App\Services\Profile\UserProfileService;
use Illuminate\Http\Response;
use App\Traits\Api\RespondsWithHttpStatus;
use GuzzleHttp\Exception\BadResponseException;
use App\Models\User;
class AuthController extends Controller
{
    use RespondsWithHttpStatus;

    public function register(RegisterRequest $request, AuthService $authService)
    {
       
        try {
            $email_token = sha1(time());
            $request['token'] = $email_token;
            $user = $authService->saveUser($request);
            $data = json_decode($authService->getAccessToken($request));
            return $this->success(
                'Successfully Registered',
                ['token' => $data, 'user' => $user],
                Response::HTTP_OK
            );
        } catch (\Exception $exception) {
            return $this->failure(
                $exception->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );

        }
    }

    public function login(LoginRequest $request, AuthService $authService)
    {
        try {
            $data = json_decode($authService->getAccessToken($request));

        } catch (BadResponseException $exception) {
            return $this->failure(
                "Unauthorized",
                Response::HTTP_UNAUTHORIZED
            );
        }
        $user = User::whereEmail($request->email)->first();
        $user['access_token'] = $data->access_token;
        $user['expired_at'] = $data->expires_in;
        $user['refresh_token'] = $data->refresh_token;
        return $this->success(
            'Successfully login',
            ['token' => $data, 'user' => $user],
            Response::HTTP_OK
        );
    }

    public function refresh(Request $request, AuthService $authService)
    {
        try {
            $data = json_decode($authService->refreshToken($request));
        } catch (BadResponseException $exception) {
            return $this->failure(
                "Unauthorized",
                Response::HTTP_UNAUTHORIZED
            );
        }
        return $this->success(
            'New Access Token',
            $data,
            Response::HTTP_OK
        );
    }
    public function forgotPassword(Request $request,AuthService $authService)
    {
        $request->validate([
            'email' => 'required|string|email:rfc|regex:/^([a-zA-Z0-9\.]+)@([a-zA-Z0-9\.]+)\.([a-zA-Z]{2,5})$/|max:255',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);
        try {
            $response = $authService->resetPassword($request);
            if($response){
                return $this->success('Password reset Successfully.', 200);
            } else {
                return $this->failure('Email given is Wrong', 500);
            }

        } catch (\Exception $exception) {
            return $this->failure('Internal Server Error! Something went wrong.', 500);
        }
    }
    
}
