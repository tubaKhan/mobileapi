<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Auth\AuthService;
use App\Services\Profile\UserProfileService;
use App\Traits\Api\RespondsWithHttpStatus;
use App\Models\User;
use App\Http\Requests\Api\User\UserProfileRequest;
class UserController extends Controller
{
    use RespondsWithHttpStatus;
    
    public function saveUserProfile(UserProfileRequest $request, UserProfileService $userProfileService)
    {
        try {
            $userProfile = $userProfileService->saveUserProfile($request);
            return $this->success('User Profile saved Successfully.', $userProfile, 200);
        }
        catch (\Exception $exception) {
            return $this->failure('Internal Server Error! Something went wrong.', 500);
        }
    }
    public function userProfile(UserProfileService $userProfileService)
    {
        try {
            $userProfile = $userProfileService->userProfile();
            return $this->success('User Profile ', $userProfile, 200);
        }
        catch (\Exception $exception) {
            return $this->failure('Internal Server Error! Something went wrong.', 500);
        }
    }

}
