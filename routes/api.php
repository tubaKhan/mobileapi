<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    // Register & Login Requests
    Route::group(['prefix' => 'auth'], function() {
        Route::post('register', 'Api\AuthController@register');
        Route::post('login', 'Api\AuthController@login')->name('login');
        Route::post('forgot-password', 'Api\AuthController@forgotPassword')->name('forgotPassword');
    });
    
    Route::group(['middleware' => 'auth:api','prefix' => 'auth' ], function() {
        //User Profile routes
        Route::group(['prefix' => 'user'], function() {
            Route::post('/','Api\UserController@userProfile');
            Route::post('profile','Api\UserController@saveUserProfile');
        });
        //Category Routes
        Route::group(['prefix' => 'category'], function() {
            Route::post('/','Api\CategoryController@index');
        });
        //Product Routes
        Route::group(['prefix' => 'product'], function() {
            Route::post('/','Api\ProductController@index');
            Route::post('category-wise','Api\ProductController@categoryWiseProducts');
        });
      
    });
